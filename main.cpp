/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/
#include <iostream>

#include "LASFiles.h"
#include "PointClouds.h"
#include "Hydro.h"
#include "UTM.h"

using namespace std;

int main(int argc,char *argv[]){

/*	int TIME=1;
	double INFIL=0;
	ofstream RR("E:/Data/RR.txt",ios::out);
	RR.setf(ios::fixed,ios::floatfield);
	for(double RainFallRate=0;RainFallRate<1;RainFallRate+=.01){
		for(int TYP=0;TYP<11;TYP++){
			int h=GreenAmpt(0.01*RainFallRate/TIME,TIME,0,TYP,&INFIL);
			RR<<h<<"\t"<<TYP<<"\t"<<INFIL<<"\t"<<RainFallRate<<"\n";
		}
	}
	RR.close();
	return 0;
*/
	double PET=CalcPotentialEvap(38.4687500000276,24,180);
	if(argv[1]==NULL){
		cout<<"************************************************\n";
		cout<<"* A hydrological model based LIDAR point clouds*\n";
		cout<<"************************************************\n";
		cout<<"\nAUTHOR: Andrew J Gallagher\n";
		cout<<"Released under the MIT/X11 License\n";
		cout<<"Usage:\n";
		cout<<"\tFloodMap {PARMS} FILENAME\n";
		cout<<"\n";
		cout<<"\tInput Parameters\n";
		cout<<"\t\ttime\t\tduration of storm (hours)\n";
		cout<<"\t\train\t\tAmount of Rain (meters)\n";
		cout<<"\t\thres\t\tHorizontal binning resolution(meters)\n";
		cout<<"\t\tzres\t\tVertical binning resolution(meters)\n";
		cout<<"\t\tBEK\t\tBare earth kernel radius (bins)\n";
		cout<<"\t\tSMAP\t\tSoil Map\n";
		cout<<"\t\t{FILENAME}\tName and path to LAS file\n";
		cout<<"Example:\nFloodMap.exe hres=4 zres=1 rain=0.03 time=2 BEK=2 SMAP=C:/SOILMAP C:/PointCloud.las\n\n";
		cout<<"\n\n***************************LICENSE INFO*********************************\n";
		cout<<"Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)\n";
		cout<<"Permission is hereby granted, free of charge, to any person obtaining a copy\n";
		cout<<"of this software and associated documentation files (the \"Software\"), to\n";
		cout<<"deal in the Software without restriction, including without limitation the\n";
		cout<<"rights to use, copy, modify, merge, publish, distribute, sublicense, and/or\n";
		cout<<"sell copies of the Software, and to permit persons to whom the Software is\n";
		cout<<"furnished to do so, subject to the following conditions:\n\n";

		cout<<"The above copyright notice and this permission notice shall be included in\n";
		cout<<"all copies or substantial portions of the Software.\n\n";

		cout<<"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n";
		cout<<"IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n";
		cout<<"FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n";
		cout<<"AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n";
		cout<<"LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n";
		cout<<"OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN\n";
		cout<<"THE SOFTWARE.\n";
	}else{
		double RES[3];
		double BEK=2;
		RES[0]=-1;
		RES[1]=-1;
		RES[2]=-1;
		// Parse command line parameters
		char temp[1024];
		char *pch;
		int ParCnt=0;
		int FileCnt=0;
		double TIME=0;
		double RAINFALL=0;
		char SMAPNAME[1024];
		for(int i=1;i<argc;i++){
			strcpy(temp,argv[i]);
			pch=strtok(temp,"=");
			if(strcmp(temp,"hres")==0){
				pch=strtok(NULL,"=");
				RES[0]=atof(pch);
				RES[1]=RES[0];
				ParCnt++;
			}else if(strcmp(temp,"zres")==0){
				pch=strtok(NULL,"=");
				RES[2]=atof(pch);
				ParCnt++;
			}else if(strcmp(temp,"time")==0){
				pch=strtok(NULL,"=");
				TIME=atof(pch);
				ParCnt++;
			}else if(strcmp(temp,"rain")==0){
				pch=strtok(NULL,"=");
				RAINFALL=atof(pch);
				ParCnt++;
			}else if(strcmp(temp,"BEK")==0){
				pch=strtok(NULL,"=");
				BEK=atof(pch);
				ParCnt++;
			}else if(strcmp(temp,"SMAP")==0){
				pch=strtok(NULL,"=");
				strcpy(SMAPNAME,pch);
				ParCnt++;
			}else if(strpbrk(temp,".las")!=NULL){
				FileCnt++;
			}
		}
		if(RAINFALL==0){
			RAINFALL=0.2;
		}
		if(TIME==0){
			TIME=2;
		}
		if(RES[0]==-1){
			RES[0]=4;
		}
		if(RES[1]==-1){
			RES[1]=4;
		}
		if(RES[2]==-1){
			RES[2]=1;
		}

		if(FileCnt==0){
			cout<<"Cannot regonize file\n";
			return -1;
		}

		LASFile INPUT;
		strcpy(INPUT.FileName,argv[ParCnt+1]);

		cout<<"Reading Data...\n";
		INPUT.ReadLASData();

		// Begin gridding data
		cout<<"Gridding Data...\n";
		unsigned long int NBINS[3];
		double MAXS[3];
		double MINS[3];		

		MAXS[0]=INPUT.Header.x_max;
		MAXS[1]=INPUT.Header.y_max;
		MAXS[2]=INPUT.Header.z_max;
		MINS[0]=INPUT.Header.x_min;
		MINS[1]=INPUT.Header.y_min;
		MINS[2]=INPUT.Header.z_min;

		// VOXEL_N being used
		//	Currently, 2 extra fields 
		//		1st		Point Intensity
		//		2nd		Water Flow Path
		//		3rd		Water Content
		//
		GetNumBins(MINS,MAXS,RES,NBINS);
		cout<<"Data binned at "<< RES[0]<<","<<RES[1]<<","<<RES[2]<<" meters\n";
		cout<<"Number of bins is ["<<NBINS[0]<<","<<NBINS[1]<<"]\n";
		LinkedList<VOXEL_N> **GRID=AllocateGrid<VOXEL_N>(NBINS[0],NBINS[1]);

		BinDataXY(INPUT.data,INPUT.Header.num_points,GRID,MINS,RES,NBINS);
		
		WriteXYZFile(GRID,NBINS,"E:/DATA/tmpTestbe.xyz",NULL);
		FillVoids(GRID,RES,NBINS,MINS);
		WriteXYZFile(GRID,NBINS,"E:/DATA/tmpTestbe2.xyz",NULL);

		// Done with LAS data, close it out
		INPUT.~LASFile();

		cout <<"Writing Binned Result...\n";
		char OUT_DIR[1024];
		sprintf(OUT_DIR,"E:/DATA/ModelResults/tmp4/");

		char BIN_NAME[1024];
		sprintf(BIN_NAME,"%s%0.2fx%0.2fm_%0.2fhrs_%0.2frain_BINS.xyz",OUT_DIR,RES[0],RES[2],TIME,RAINFALL);
		WriteXYZFile(GRID,NBINS,BIN_NAME,NULL);

		// Make bare earth
		cout<<"Estimating Bare Earth...\n";
		LinkedList<VOXEL_N> **BE=AllocateGrid<VOXEL_N>(NBINS[0],NBINS[1]);
		QuasiBareEarth(GRID,BE,NBINS,BEK);

//		WriteXYZFile(BE,NBINS,"E:/DATA/tmpTestbe.xyz",NULL);

		// Read soil map. Fit within the bounds of the data
		//ifstream SM("E:/DATA/Soil/SoilMap.img",ios::in);
		ifstream SM(SMAPNAME,ios::in);
		int SoilMap[313][313];
		unsigned short int t;
		for(int i=0;i<313;i++){
			for(int j=0;j<313;j++){
				SM.read((char*)&t,sizeof(unsigned short int));
				SoilMap[j][312-i]=t;
			}
		}
		SM.close();
		double SMstep[2];
		SMstep[0]=(INPUT.Header.x_max-INPUT.Header.x_min)/313;
		SMstep[1]=(INPUT.Header.y_max-INPUT.Header.y_min)/313;

		for(int i=0;i<NBINS[0];i++){
			for(int j=0;j<NBINS[1];j++){
				if(BE[i][j].StPtr!=NULL){
					int sxInd=(BE[i][j].StPtr->data.x-MINS[0])/SMstep[0];
					int syInd=(BE[i][j].StPtr->data.y-MINS[1])/SMstep[1];
					BE[i][j].StPtr->data.XF[0]=SoilMap[sxInd][syInd];
				}
			}
		}

		// Do the water analysis
		//	POOL (XF set in WaterFlow)
		//		currently 1 extra field (2nd to be added soon)
		//			1st		Z index of point
		//			2nd		Water Content
		//
		cout<<"Performing Hydrological Analysis...\n";
		LinkedList<VOXEL_N> **POOL=AllocateGrid<VOXEL_N>(NBINS[0],NBINS[1]);
		WaterFlow(BE,POOL,NBINS,RES,MINS,SoilMap,SMstep,RAINFALL,TIME);
		
		
		cout<<"Writing Pools...\n";
		int RGB[3];
		RGB[0]=51;RGB[1]=102;RGB[2]=255;
		char POOL_NAME[1024];
		sprintf(POOL_NAME,"%s%0.2fx%0.2fm_%0.2fhrs_%0.2frain_POOLS.xyz",OUT_DIR,RES[0],RES[2],TIME,RAINFALL);
		WriteXYZFile(POOL,NBINS,POOL_NAME,RGB);
		cout<<"\nWriting Bare Earth estimate...\n";
		char BE_NAME[1024];
		sprintf(BE_NAME,"%s%0.2fx%0.2fm_%0.2fhrs_%0.2frain_BE.xyz",OUT_DIR,RES[0],RES[2],TIME,RAINFALL);
		WriteXYZFile(BE,NBINS,BE_NAME,NULL);
		DeAllocateGrid(POOL,NBINS[0]);
		DeAllocateGrid(BE,NBINS[0]);
		DeAllocateGrid(GRID,NBINS[0]);
	}

	return 0;
}