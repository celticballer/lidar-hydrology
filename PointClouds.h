/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/

/*!%**********************************************************************

	A collection of basic point cloud related things. Is based on the
	LinkedList class defined in LinkedList.h

**********************************************************************%!*/

#ifndef POINTCLOUDS_H
#define POINTCLOUDS_H

#include <math.h>
#include <fstream>

#include "LinkedList.h"
#include "Gally.h"

/*!
	class VOXEL_BASIC

	Description
		Used for representing the most basic of voxels (XYZ positions).
		In a lot of cases, this is good enough. Notice that all variables
		are public. I didn't see any need to make them private and create 
		GET and SET operators. This class is intended to be like a normal
		variable, just with three values, so lets keep it as such. 
	
	Variables
		double	x		X location
		double	y		Y location
		double	z		Z location
	
	Functions
		Pretty much just the standard class functions (no destructor, nothing
		out of the ordinary to destroy)
	
		VOXEL_BASIC()
		VOXEL_BASIC(const VOXEL_BASIC &V)					default constructor, sets all to 0
		const VOXEL_BASIC &operator=(const VOXEL_BASIC V)	overload the '=' operator
		bool operator==(const VOXEL_BASIC &V)const			overload the '==' operator
		bool operator!=(const VOXEL_BASIC &V)const			overload the '!=' operator
!*/
class VOXEL_BASIC{
	public:
		double	x;		
		double	y;		
		double	z;	
	
		VOXEL_BASIC(){
			x=0;y=0;z=0;
		}
		VOXEL_BASIC(const VOXEL_BASIC &V){
			x=V.x;
			y=V.y;
			z=V.z;
		}
		const VOXEL_BASIC &operator=(const VOXEL_BASIC V){
			if(this!=&V){
				this->z=V.z;
				this->x=V.x;
				this->y=V.y;
			}
			return *this;
		}
		bool operator==(const VOXEL_BASIC &V)const{
			return ((this->x==V.x)&&(this->y==V.y)&&(this->z==V.z));
		}
		bool operator!=(const VOXEL_BASIC &V)const{
			return !(*this==V);
		}
};
/*!
	class VOXEL_4

	Description
		Often times, there are more values associated with a voxel 
		other than simply its position. In 90% of the vases I've dealt
		with, 1 extra value is enough. This class provides that funcionality.

	Values
		(inherited from VOXEL_BASIC)
		double f	one extra value 

	Functions
		VOXEL_4()								Constructor
		VOXEL_4(const VOXEL_4)					Copy constructor	
		const VOXEL_4 &operator=(const VOXEL_4)	overload '='

	TODO
		overload '==' and '!='
!*/
class VOXEL_4:public VOXEL_BASIC{
	public:
		double f;	
		
		VOXEL_4(){
			f=0;
		}
		VOXEL_4(const VOXEL_4 &V){
			this->VOXEL_BASIC::operator =(V);
			f=V.f;
		}
		const VOXEL_4 &operator=(const VOXEL_4 V){
			if(this!=&V){
				this->VOXEL_BASIC::operator =(V);
				this->f=V.f;
			}
			return *this;
		}
};
/*!
	class VOXEL_N

	Description
		For those cases where 1 extra field simply isnt enough, this provides
		the ability for any number of extra fields to be added.

	Values
		(Inherited from VOXEL_BASIC)
		double *XF		a dynamic array for any number of extra fields

	Functions
		VOXEL_N()									Constructor sets values to 0 and XF to NULL
		VOXEL_N(int SZ)								Same as above, except it allocates the # of extra fields
		VOXEL_N(const VOXEL_N &V)					Copy constructor
		~VOXEL_N()									Destructor, deallocates memory used in extra fields
		void AllocateXFmem(int n)					Reserves 'n' values for extra field use
		void DeAllocateXFmem()						Deletes the extra field array
		const VOXEL_N &operator=(const VOXEL_N V)	overload '=' operator

	TODO
		overload '==' and '!='
!*/
class VOXEL_N:public VOXEL_BASIC{
	public:
		double	*XF;	
		int		nXF;	
		
		VOXEL_N(){
			z=0;x=0;y=0;nXF=0;
			XF=NULL;
		}
		VOXEL_N(int SZ){
			z=0;x=0;y=0;nXF=SZ;
			XF=NULL;
			AllocateXFmem(SZ);
		}
		VOXEL_N(const VOXEL_N &V){
			this->VOXEL_BASIC::operator =(V);
			this->nXF=V.nXF;
			if(V.XF!=NULL){
				this->XF=new double[nXF];
				for(int i=0;i<this->nXF;i++){
					this->XF[i]=V.XF[i];
				}
			}else{
				this->XF=NULL;
			}
		}
		~VOXEL_N(){
			DeAllocateXFmem();
		}
		
		// Deals with allocation and deallocation of extra field memory
		void AllocateXFmem(int n){	
									if(XF!=NULL){
										delete [] XF;
										XF=NULL;
									}
									nXF=n;
									XF=new double[nXF];
									memset(XF,0,sizeof(double)*nXF);
								}
		void DeAllocateXFmem(){		if(XF!=NULL){
										delete [] XF;
										XF=NULL;
										nXF=0;
									}
								}
		
		const VOXEL_N &operator=(const VOXEL_N V){
			if(this!=&V){
				this->VOXEL_BASIC::operator =(V);
				if(V.nXF>0){
					this->nXF=V.nXF;
					this->AllocateXFmem(this->nXF);
					for(int i=0;i<this->nXF;i++){
						this->XF[i]=V.XF[i];
					}
				}else{
					this->XF=NULL;
				}
			}
			return *this;
		}
};
/*!%**********************************************************************

Grid Functions
	The following functions apply a grid to a 3D data set as described
	here. 

	The 3D data is read out of the file in a double array representing
	the XYZ points and associated values (1 column for each dimension). 

	The grid is applied using a linked list method where an NxM 2D grid 
	is created and the z values are added in the approriate XY position
	as a linked list.

***********************************************************************%!/

/*!
	Function: AllocateGrid()

	Description
		
	INPUTS
		long int XSZ		X dimension of the grid
		long int YSZ		Y dimention of the grid

	OUTPUTS
		NONE

	RETURNS
		LinkedList<T> **	A point to the grid
!*/
template<class T>
LinkedList<T>** AllocateGrid(long int XSZ,long int YSZ){
	
	LinkedList<T> **GRID=new LinkedList<T>*[XSZ];
	for(int i=0;i<XSZ;i++){
		GRID[i]=new LinkedList<T>[YSZ];
	}
	return GRID;
}
/*!
	Function: DeAllocateGrid()

	Description
		
	INPUTS
		long int XSZ		X dimension of the grid

	OUTPUTS
		NONE

	RETURNS
		NONE
!*/
template<class T>
void DeAllocateGrid(LinkedList<T>** GRID,long int XSZ){
	for(int i=0;i<XSZ;i++){
		delete [] GRID[i];
	}
	delete [] GRID;
}
/*!
	Function: GetNumBins()

	Description
		
	INPUTS
		double MINS
		double MAXS
		double RES

	OUTPUTS
		unsigned long int NBINS

	RETURNS
		NONE
!*/
void GetNumBins(double *MINS, double *MAXS,double *RES,unsigned long int *NBINS){
	long int xMaxB=(long int)floor(MAXS[0]/RES[0])+1;
	long int xMinB=(long int)floor(MINS[0]/RES[0])-1;
	long int yMaxB=(long int)floor(MAXS[1]/RES[1])+1;
	long int yMinB=(long int)floor(MINS[1]/RES[1])-1;
	long int zMaxB=(long int)floor(MAXS[2]/(RES[2]))+1;
	long int zMinB=(long int)floor(MINS[2]/(RES[2]))-1;
	NBINS[0]=xMaxB-xMinB;
	NBINS[1]=yMaxB-yMinB;
	NBINS[2]=zMaxB-zMinB;
}
/*!
	Function: CheckBounds()

	Description
		
	INPUTS
		x		x location	
		y		y location
		NB		Number of bins
	
	OUTPUTS
		NONE	

	RETURNS
		1	if in bounds
		0	if out of bounds
!*/
bool CheckBounds(unsigned long int x,unsigned long int y,unsigned long int *NB){
	return (x>=0)&&(x<NB[0])&&(y>=0)&&(y<NB[1]);
}
bool CheckBounds(unsigned long int x,unsigned long int y,double **XL,double **YL){
	return ((x>=YL[0][y])&&(x<=YL[1][y])&&(y>=XL[0][x])&&(y<=XL[1][x]));
}
/*!
	function BinDataXY()

	Description

	INPUTS
		D		2D double array contianing the XYZ(etc) columns of the data
		NPTS	Total number of points in D
		MINS	Min values for each column of D
		RES		Resolution which data is binned
		NBINS	Number of bins being created

	OUTPUTS
		G		A grid containing the binned data

	RETURNS
		NONE
!*/
template <class T>
void BinDataXY(double **D,unsigned long int NPTS,LinkedList<T> **G,double *MINS,double *RES,unsigned long int *NBINS){

	T tmpPt(3);
	for(unsigned long int nP=0;nP<NPTS;nP++){
		long int xind=(long int)floor((D[0][nP]-MINS[0])/RES[0]+0.5);
		long int yind=(long int)floor((D[1][nP]-MINS[1])/RES[1]+0.5);
		
		tmpPt.x=xind*RES[0]+MINS[0];//D[0][nP];
		tmpPt.y=yind*RES[1]+MINS[1];//D[1][nP];
		tmpPt.z=D[2][nP];
		tmpPt.XF[0]=D[3][nP];
		tmpPt.XF[1]=0;
		tmpPt.XF[2]=0;
		if(G[xind][yind].StPtr==NULL){
			G[xind][yind].AddNode(tmpPt);
		}else{
			if(tmpPt.z<G[xind][yind].StPtr->data.z){
				G[xind][yind].StPtr->data=tmpPt;
			}
		}
	}
/*	double zmax=17.41;
	double zres=1;
	long int zMaxB=(long int)floor(zmax/zres)+1;
	long int zMinB=(long int)floor(MINS[2]/zres)-1;
	int zbins=zMaxB-zMinB;
	Node<T> *tmpPtr;
	long int EVE=0;
	long int EV2=0;
	for(int i=0;i<NBINS[0];i++){
		for(int j=0;j<NBINS[1];j++){
			tmpPtr=G[i][j].StPtr;
			int maxind=(-1000);
			if(tmpPtr!=NULL){
				long int tmpInd=(long int)floor((tmpPtr->data.z-MINS[2])/zres+0.5);
				if(tmpInd>maxind){maxind=tmpInd;}
				tmpPtr=tmpPtr->next;
			}else{
				EV2++;
			}
			if(maxind!=(-1000)){
				EVE=EVE+(zbins-maxind);
			}
		}
	}*/
}
/*!
	function BinDataXY_avg()

	Description
		same is BinDataXY, except the voxel position is the average of all 
		poitns placed in it

	INPUTS
		D		2D double array contianing the XYZ(etc) columns of the data
		NPTS	Total number of points in D
		MINS	Min values for each column of D
		RES		Resolution which data is binned
		NBINS	Number of bins being created

	OUTPUTS
		G		A grid containing the binned data

	RETURNS
		NONE
!*/
template <class T>
void BinDataXY_avg(double **D,unsigned long int NPTS,LinkedList<T> **G,double *MINS,double *RES,unsigned long int *NBINS){

	T tmpPt(3);
	for(unsigned long int nP=0;nP<NPTS;nP++){
		long int xind=(long int)floor((D[0][nP]-MINS[0])/RES[0]+0.5);
		long int yind=(long int)floor((D[1][nP]-MINS[1])/RES[1]+0.5);
		
		tmpPt.x=D[0][nP];
		tmpPt.y=D[1][nP];
		tmpPt.z=D[2][nP];
		tmpPt.XF[0]=D[3][nP];
		tmpPt.XF[1]=0;
		tmpPt.XF[2]=0;
		G[xind][yind].AddNode(tmpPt);
	}

	LinkedList<T> **tmpg=AllocateGrid<VOXEL_N>(NBINS[0],NBINS[1]);
	
	Node<T> *tmpPtr;
	for(int x=0;x<NBINS[0];x++){
		for(int y=0;y<NBINS[0];y++){
			if(G[x][y].StPtr!=NULL){
				SortZColumn(G[x][y],0);
				tmpPtr=G[x][y].StPtr;
				int npts=0;
				int zI=(long int)floor((tmpPtr->data.z-MINS[1])/RES[1]+0.5);;
				int zI_last=zI;
				int tmpz=0;
				while(tmpPtr!=NULL){
					zI=(long int)floor((tmpPtr->data.z-MINS[1])/RES[1]+0.5);
					if(zI!=zI_last){
						tmpPt.z=
					}
					tmpz+=tmpPtr->data.z;
					npts++;
					zI_last=zI;
					tmpPtr=tmpPtr->next;
				}
			}
		}
	}
	DeAllocateGrid(tmpg,NBINS[0]);
}
/*
	function SortZColumn()

	Description

	INPUTS
		C		column to sort
		dir		0=descending, 1=ascending

	OUTPUTS
		Sorted Column

	RETURNS
		NONE
!*/
template<class T>
void SortZColumn(LinkedList<T> *C,int dir){
	T tmp;
	Node<T> *tmpPtr;
	bool swap=true;
	int cnt=0;
	while(1){
		swap=false;
		tmpPtr=C->StPtr;
		while(tmpPtr->next!=NULL){
			if(tmpPtr->data.z>tmpPtr->next->data.z){
				tmp=tmpPtr->data;
				tmpPtr->data=tmpPtr->next->data;
				tmpPtr->next->data=tmp;
				swap=true;
			}
			tmpPtr=tmpPtr->next;
		}
		if(swap==false){
			break;
		}
		cnt++;
	}
}
/*!%**********************************************************************

	FILE I/O Functions
	
	The following functions handle writing poitns to file 

***********************************************************************%!/
/*
	function WriteXYZFile()

	Description

	INPUTS
		G		The grid to write
		NBINS	The number of bins in the grid
		FNAME	Full path including file name to write

	OUTPUTS
		NONE

	RETURNS
		NONE
!*/
template<class T>
void WriteXYZFile(LinkedList<T> **G,unsigned long int *NBINS,char *FNAME,int *RGB){
	ofstream OUT(FNAME,ios::out);
	OUT.setf(ios::fixed);
	OUT.precision(16);
	Node<T> *tmpPtr;
	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=G[x][y].StPtr;
			while(tmpPtr!=NULL){
				OUT<<(double)tmpPtr->data.x<<"\t";
				OUT<<(double)tmpPtr->data.y<<"\t";
				OUT<<(double)tmpPtr->data.z<<"\t";
				if(tmpPtr->data.nXF>0){
					for(int i=0;i<tmpPtr->data.nXF;i++){
						OUT<<(double)tmpPtr->data.XF[i]<<"\t";
					}
				}
				if(RGB!=NULL){
					OUT<<RGB[0]<<"\t"<<RGB[1]<<"\t"<<RGB[2];
				}
				OUT<<"\n";
				tmpPtr=tmpPtr->next;
			}
		}
	}
}
/*!%*********************************************************************

	Processing/Exploitation Function
	
	The following functions manipulate the data in some way 

***********************************************************************%!*/

/*
	function QuasiBareEarth()

	Description
		A rudimentary bare earth function where that computes the ground
		level of a point cloud by taking the lowest return in each z 
		column and replacing it with the neighborhood median value

	INPUTS

	OUTPUTS

	RETURNS

!*/
template<class T>
void QuasiBareEarth(LinkedList<T> **G,LinkedList<T> **BE, unsigned long int *NBINS, int HOOD){

	T tmp;
	Node<T> *tmpPtr;
	double Nv=0;
	LinkedList<T> **tmpBin=AllocateGrid<T>(NBINS[0],NBINS[1]);

	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=G[x][y].StPtr;
			if(tmpPtr!=NULL){
				Nv=tmpPtr->data.z;
				for(int nx=-HOOD;nx<=HOOD;nx++){
					for(int ny=-HOOD;ny<=HOOD;ny++){
						if(CheckBounds(x+nx,y+ny,NBINS)){
							if(G[x+nx][y+ny].StPtr!=NULL){
								if(G[x+nx][y+ny].StPtr->data.z<Nv){
									Nv=G[x+nx][y+ny].StPtr->data.z;
								}
							}
						}
					}
				}
				tmp=tmpPtr->data;
				tmp.z=Nv;
				tmpBin[x][y].AddNode(tmp);
			}
		}
	}
	double *pts=new double[(2*HOOD+1)*(2*HOOD+1)];
	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=tmpBin[x][y].StPtr;
			if(tmpPtr!=NULL){
				int cnt=0;
				memset(pts,0,sizeof(double)*(2*HOOD+1)*(2*HOOD+1));
				double MAXZ=1e-10;
				double MINZ=1e10;
				for(int nx=-HOOD;nx<=HOOD;nx++){
					for(int ny=-HOOD;ny<=HOOD;ny++){
						if(CheckBounds(x+nx,y+ny,NBINS)){
							if(tmpBin[x+nx][y+ny].StPtr!=NULL){
								pts[cnt]=tmpBin[x+nx][y+ny].StPtr->data.z;
								cnt++;
							}
						}
					}
				}
				
				tmp=tmpPtr->data;
				tmp.z=Median(pts,cnt);
				BE[x][y].AddNode(tmp);
			}
		}
	}
	delete [] pts;
	DeAllocateGrid(tmpBin,NBINS[0]);
}
/*
	function FindConnectedNeighbors()

	Description
		Finds all the points of a connected area

	INPUTS

	OUTPUTS

	RETURNS
*/
template<class T>
void FindConnectedNeighbors(LinkedList<T> **A,T CurPt,double *RES,unsigned long int *NBINS,double *MINS,LinkedList<T> *CPTS){

	Node<T> *tmpPtr,*tmpPtr2;
	T tmpPt;

	CPTS->AddNode(CurPt);
	tmpPtr=CPTS->StPtr;
	while(1){
		bool addpt=false;
		
		for(int i=-1;i<=1;i++){
			for(int j=-1;j<=1;j++){
				long int xb=(long int)floor((tmpPtr->data.x-MINS[0])/RES[0]+0.5);
				long int yb=(long int)floor((tmpPtr->data.y-MINS[1])/RES[1]+0.5);

				if(CheckBounds(xb+i,yb+j,NBINS)){
					if(A[xb+i][yb+j].StPtr!=NULL){
						// if any of this points neighbors are null, add it
						/*bool isnull=false;
						for(int ii=-1;ii<=1;ii++){
							for(int jj=-1;jj<=1;jj++){
								if(CheckBounds(xb+i+ii,yb+j+jj,NBINS)){
									if(A[xb+i+ii][yb+j+jj].StPtr==NULL){
										isnull=true;
										break;
									}
								}
							}
							if(isnull==true){
								break;
							}
						}
						*/
						//if(isnull==true){
							// check to see if its already listed
							tmpPtr2=CPTS->StPtr;
							bool listed=false;
							while(tmpPtr2!=NULL){
								if((tmpPtr2->data.x==A[xb+i][yb+j].StPtr->data.x)&&(tmpPtr2->data.y==A[xb+i][yb+j].StPtr->data.y)){
									listed=true;
									break;
								}
								tmpPtr2=tmpPtr2->next;
							}
							if(listed==false){
								CPTS->AddNode(A[xb+i][yb+j].StPtr->data);
								addpt=true;
							}
						//}
					}
				}
			}
		}
		if(addpt==false){
			break;
		}
		tmpPtr=tmpPtr->next;
	}

}
/*
	function GetDataBounds()

	Description
		Gets the XY data bounds for a point cloud

	INPUTS

	OUTPUTS

	RETURNS

!*/
template<class T>
void GetDataBounds(LinkedList<T> **G,unsigned long int *NBINS,double **XLIMS,double **YLIMS){
	Node<T> *tmpPtr;

	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=G[x][y].StPtr;
			if(tmpPtr!=NULL){
				if(y<XLIMS[0][x]){XLIMS[0][x]=y;}
				if(y>XLIMS[1][x]){XLIMS[1][x]=y;}
				if(x<YLIMS[0][y]){YLIMS[0][y]=x;}
				if(x>YLIMS[1][y]){YLIMS[1][y]=x;}
			}
		}
	}
}
/*
	function FillVoids()

	Description
		Finds and fills in voids in a grid

	INPUTS

	OUTPUTS

	RETURNS

!*/
template<class T>
void FillVoids(LinkedList<T> **G,double *RES,unsigned long int *NBINS,double *MINS){

	Node<T> *tmpPtr;
	T tmpPt(1);

	// First define the actual boundries of the data
	double **xlims=new double*[2];
	for(int i=0;i<2;i++){
		xlims[i]=new double[NBINS[0]];
		memset(xlims[i],0,sizeof(double)*NBINS[0]);
	}
	double **ylims=new double*[2];
	for(int i=0;i<2;i++){
		ylims[i]=new double[NBINS[1]];
		memset(ylims[i],0,sizeof(double)*NBINS[1]);
	}
	for(int i=0;i<NBINS[0];i++){
		xlims[0][i]=1e23;
		xlims[1][i]=-1e23;
	}
	for(int i=0;i<NBINS[1];i++){
		ylims[0][i]=1e23;
		ylims[1][i]=-1e23;
	}
	
	GetDataBounds(G,NBINS,xlims,ylims);

	// A void is a missing point within the boundries defined as an empty
	// area surrounded by data and/or a boundry
	ofstream BNDS("E:/DATA/bnds.xyz",ios::out);
	BNDS.setf(ios::fixed,ios::floatfield);
	LinkedList<T> **V=AllocateGrid<T>(NBINS[0],NBINS[1]);
	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=G[x][y].StPtr;
			if(tmpPtr==NULL){
				tmpPt.x=x*RES[0]+MINS[0];
				tmpPt.y=y*RES[1]+MINS[1];
				tmpPt.z=1;
				//if(((y>=xlims[0][x])&&(y<=xlims[1][x]))||((x>=ylims[0][y])&&(x<=ylims[1][y]))){
				if(CheckBounds(x,y,xlims,ylims)){
					V[x][y].AddNode(tmpPt);		
					BNDS<<x*RES[0]<<"\t";
					BNDS<<y*RES[1]<<"\t";
					BNDS<<1<<"\t0 255 0\n";
				}else{
					BNDS<<x*RES[0]<<"\t";
					BNDS<<y*RES[1]<<"\t";
					BNDS<<1<<"\t255 0 0\n";
				}
			}else{
				BNDS<<x*RES[0]<<"\t";
				BNDS<<y*RES[1]<<"\t";
				BNDS<<1<<"\t205 201 201\n";
			}
		}
	}
	BNDS.close();
	// for all the voids, interpolate the data somehow
	T tmpPt2(3);
	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			tmpPtr=V[x][y].StPtr;
			if(tmpPtr!=NULL){
				LinkedList<T> cpts;
				Node<T> *cpPtr;
				// Get all points encompassing the area
				FindConnectedNeighbors(V,tmpPtr->data,RES,NBINS,MINS,&cpts);

				// Now shrink the area until all the points are filled
				cpPtr=cpts.StPtr;
				// set each connected point with a valid neighbor to 
				// the mean of the neighboring values
				int cnt=0;
				double nm=0;
				while(1){
					while(cpPtr!=NULL){
						bool remstart=false;
						long int xb=(long int)floor((cpPtr->data.x-MINS[0])/RES[0]+0.5);
						long int yb=(long int)floor((cpPtr->data.y-MINS[1])/RES[1]+0.5);
						for(int i=-1;i<=1;i++){
							for(int j=-1;j<=1;j++){
								if(CheckBounds(xb+i,yb+j,NBINS)){
									if(G[xb+i][yb+j].StPtr!=NULL){
										nm+=G[xb+i][yb+j].StPtr->data.z;
										cnt++;
									}
								}
							}
						}
						if(cnt>0){
							if(G[xb][yb].StPtr==NULL){
								nm/=cnt;
								tmpPt2.x=cpPtr->data.x;
								tmpPt2.y=cpPtr->data.y;
								tmpPt2.z=nm;
								tmpPt2.XF[0]=cpPtr->data.XF[0];
								G[xb][yb].AddNode(tmpPt2);
							}

							// remove this point from the list
							if(cpPtr==cpts.StPtr){
								remstart=true;
							}
							cpPtr=cpts.DelNode(cpPtr);
							
						}
						if(remstart=false){
							cpPtr=cpPtr->next;
						}
						nm=0;
						cnt=0;
					}
					if(cpts.StPtr==NULL){
						break;
					}
				}
			}
		}
	}
	
	WriteXYZFile(V,NBINS,"E:/DATA/voids.xyz",NULL);
	WriteXYZFile(G,NBINS,"E:/DATA/remvoids.xyz",NULL);
	DeAllocateGrid(V,NBINS[0]);
	for(int i=0;i<2;i++){
		delete [] xlims[i];
	}
	delete [] xlims;
	for(int i=0;i<2;i++){
		delete [] ylims[i];
	}
	delete [] ylims;
}
#endif