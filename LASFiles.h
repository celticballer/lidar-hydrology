/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/

/*************************************************************************

	Class for reading and writing LAS files. 

	See also associated LASFiles.cpp

*************************************************************************/
#ifndef LASFILES_H
#define LASFILES_H

/*
	class LAS_HEADER

	Description
		A class representing the data contained in the header of an 
		LAS file

	Functions
		LAS_HEADER()		constructor

	Variables
		All parameters as described by the LAS V1.2 description document
	
*/
class LAS_HEADER{
	public:
		LAS_HEADER();

		char file_sig[4];				// 4
		unsigned short int source_id;	// 2
		unsigned short int global_enc;  // 2
		unsigned long int guid1;		// 4
		unsigned short int guid2;		// 2
		unsigned short int guid3;		// 2
		unsigned char guid4[8];			// 8
		unsigned char ver_maj;			// 1
		unsigned char ver_min;			// 1
		char sys_ident[32];				// 32
		char generator[32];				// 32
		unsigned short int create_doy;	// 2
		unsigned short int create_year; // 2
		unsigned short int head_size;	// 2
		unsigned long int point_offset;	// 4
		unsigned long int num_var;		// 4
		unsigned char point_format;		// 1
		unsigned short int point_len;	// 2
		unsigned long int num_points;	// 4
		unsigned long int num_ret[5];	// 20
		double x_scale;					// 12*8
		double y_scale;
		double z_scale;
		double x_off;
		double y_off;
		double z_off;
		double x_max;
		double x_min;
		double y_max;
		double y_min;
		double z_max;
		double z_min;
};

/*
	class LASFile

	Description
		A class used to access the LAS file information

	Functions
		LASFile()			constructor, reserves memory based on Header info
		~LASFile()			destructor,	cleans up memory
		ReadLASHeader()		Reads the header informatio
		DisplayHeader()		Prints header info
		ReadLASData()		Reads the point information
		WriteLASData()		Writes the point information

	Variables
		Header		LAS_HEADER class 
		data		2D array containing the point information
*/
class LASFile{
	public:
		LAS_HEADER Header;
		
		double **data;
		char *FileName;
		
		LASFile(){
			data=NULL;
			FileName=new char[512];
		};
		~LASFile(){
			if(data!=NULL){
				for(int i=0;i<4;i++){
					delete [] data[i];
				}
				delete [] data;	
				data=NULL;
			}
			delete [] FileName;
		}
		void ReadLASHeader();
		void DisplayHeader();
		void ReadLASData();
		void WriteLASData(char *APPEND);
};
#endif