/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/

/*************************************************************************

	A custom doulbe linked list class

*************************************************************************/
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template<class T> class LinkedList;

/*
	class Node

	Description
		Represents a single node of a linked list. 

	Functions
		Node(T)		Constructor with custom initializer
		Node()		Constructor sets everything to NULL

	Variables
		data		the data point in the node
		next		pointer to the next node
		prev		pointer to the previous node
*/
template<class T>
class Node{
	friend class LinkedList<T>;
	public:
		Node(const T &t):data(t),next(NULL),prev(NULL){};
		Node():data(NULL),next(NULL),prev(NULL){};
		T data;
		Node *next,*prev;
};

/*
	class LinkedList

	Description
		Provides a doubly linked list

	Functions
		LinkedList()		constructor
		~LinkedList()		destructor. deletes all nodes from list
		AddNode()			adds a node to the list
		SwapNode()			swaps 2 nodes in the list
		DelNode()			deletes nodes from the list via node address

	Variables
		StPtr		First pointer in the list
		LastPtr		Last pointer in the list

*/
template<class T>
class LinkedList{
	public:
		Node<T> *StPtr,*LastPtr;
		long int NumNodes;

		LinkedList():StPtr(NULL),LastPtr(NULL),NumNodes(0){};
		~LinkedList(){	
			Node<T> *delPtr;
			while(StPtr!=NULL){
				delPtr=StPtr;
				StPtr=StPtr->next;
				delete delPtr;
			}
		}

		void AddNode(const T &element){
			Node<T> *NewPtr=NULL;
			if(StPtr==NULL){
				StPtr=new Node<T>(element);
				LastPtr=StPtr;
			}else{
				NewPtr=new Node<T>(element);
				NewPtr->prev=LastPtr;
				LastPtr->next=NewPtr;
				LastPtr=NewPtr;
			}
			NumNodes++;
		}
		void SwapNode(Node<T> *E1,Node<T> *E2){
			Node<T> *tmpPtr=new Node<T>;
			tmpPtr->data=E1->data;
			E1->data=E2->data;
			E2->data=tmpPtr->data;
			delete tmpPtr;
		}
		Node<T>* DelNode(Node<T> *element){
			Node<T> *tmpPtr;
			if(element!=NULL){
				if((element->prev!=NULL) && (element->next!=NULL)){
					element->prev->next=element->next;
					element->next->prev=element->prev;
					tmpPtr=element->prev;
					delete element;
					NumNodes--;
					return tmpPtr;
				}else if((element->prev==NULL) && (element->next==NULL)){ 
					delete StPtr;
					StPtr=NULL;
					element=StPtr;
					NumNodes--;
					return StPtr;
				}else if((element->prev==NULL) && (element->next!=NULL)){
					tmpPtr=StPtr->next;
					delete StPtr;
					StPtr=tmpPtr;
					StPtr->prev=NULL;
					element=StPtr;
					NumNodes--;
					return StPtr;
				}else if((element->prev!=NULL) && (element->next==NULL)){
					tmpPtr=element->prev;
					delete LastPtr;
					LastPtr=tmpPtr;
					LastPtr->next=NULL;
					element=LastPtr;
					NumNodes--;
					return LastPtr;
				}
			}
			tmpPtr=NULL;
			return tmpPtr;
		}
};
#endif