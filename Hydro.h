/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/
/*************************************************************************

	The functions which control the water functionality of the code.  

*************************************************************************/

#ifndef HYDRO_H
#define HYDRO_H
#include <math.h>
#include <fstream>
#include "PointClouds.h"
#include "UTM.h"

#ifndef PI
#define PI	3.1415926535897931
#endif

enum  { SAND=0,
		LOAMYSAND,
		SANDYLOAM,
		SILTLOAM,
		LOAM,
		SANDYCLAYLOAM,
		SILTYCLAYLOAM,
		CLAYLOAM,
		SANDYCLAY,
		SILTYCLAY,
		CLAY
		};

void GetSoilParameters(int SOIL_TYPE,double *THI,double *Kh,double *PSIAE,double *B){
	switch(SOIL_TYPE){
		case SAND:
			*THI=0.395;	
			*Kh=1.76e-2;	// cm/s
			*PSIAE=12.1;	// cm
			*B=4.05;		
			break;
		case LOAMYSAND:
			*THI=0.410;
			*Kh=1.56e-2;
			*PSIAE=9.0;
			*B=4.38;
			break;
		case SANDYLOAM:
			*THI=0.435;
			*Kh=3.47e-2;
			*PSIAE=21.8;
			*B=4.90;
			break;
		case SILTLOAM:
			*THI=0.485;
			*Kh=7.2e-4;
			*PSIAE=78.6;
			*B=5.30;
			break;
		case LOAM:
			*THI=0.451;
			*Kh=6.95e-4;
			*PSIAE=47.8;
			*B=5.39;
			break;
		case SANDYCLAYLOAM:
			*THI=0.420;
			*Kh=6.3e-4;
			*PSIAE=29.9;
			*B=7.12;
			break;
		case SILTYCLAYLOAM:
			*THI=0.477;
			*Kh=1.70e-4;
			*PSIAE=35.6;
			*B=7.75;
			break;
		case CLAYLOAM:
			*THI=0.476;
			*Kh=2.45e-4;
			*PSIAE=63.0;
			*B=8.52;
			break;
		case SANDYCLAY:
			*THI=0.426;
			*Kh=2.17e-4;
			*PSIAE=15.3;
			*B=10.4;
			break;
		case SILTYCLAY:
			*THI=0.492;
			*Kh=1.03e-4;
			*PSIAE=49.0;
			*B=10.4;
			break;
		case CLAY:
			*THI=0.482;
			*Kh=1.28e-4;
			*PSIAE=40.5;
			*B=11.4;
			break;
		default:
			*THI=0.420;
			*Kh=6.3e-4;
			*PSIAE=29.9;
			*B=7.12;
			break;
	}
}
/*
	function CalcPotentialEvap()
	
	Description
		Calculates potential evaporation based on Eq 7.63 from the following
		"Physical Hydrology: Second Edition", Dingman

	INPUTS
		LAT		Latitude 
		T_a		Average Daily Temperature
		Day		Day number (Jan 1st=1, Dec 31st=365)

	OUTPUTS
		
	RETURNS 
		NONE
*/
double CalcPotentialEvap(double LAT, double T_a, int DAY){
	
	double ANG_ROT=0.2618;	// angular velocity of Earth's rotation (radians)

	double day_ang=2*PI*(DAY-1)/365;			// day angle
	double d=0.006918-0.399912*cos(day_ang);	// solar declination
	d+=0.070257*sin(day_ang)-0.006758*cos(2*day_ang);
	d+=0.000907*sin(2*day_ang)-0.002697*cos(3*day_ang)+0.00148*sin(3*day_ang);
	
	double Thr=-acos(-tan(d)*tan(LAT*PI/180))/ANG_ROT;	// time of sunrise
	double Ths=acos(-tan(d)*tan(LAT*PI/180))/ANG_ROT;	// time of sunset
	double D=Ths-Thr;									// Day length
	double e=0.611*exp(17.3*T_a/(T_a+237.3));		// Saturation Vapor Pressure
	double PET=29.8*D*e/(T_a+273.2);				// Potential Evaporation (mm/day)

	return PET;
}
/*
	function GreenAmpt()
	
	Description
		Applies the explicit form of the Green and Ampt  model for water
		infiltration into soil

		Result is returned in cm

	INPUTS
		
	OUTPUTS
		
	RETURNS 
		NONE
*/
int GreenAmpt(double RAIN, double TIME,double InitWater, int SOILTYPE,double *INFIL){

	double thi=0;
	double Kh=0;
	double psi_ae=0;
	double b=0;

	// Get the above parameters for the soil type
	GetSoilParameters(SOILTYPE,&thi,&Kh,&psi_ae,&b);
	Kh*=(60*60*0.01);// convert to meters/hour
	psi_ae*=0.01;
	
	// If the water input rate is less than hydro. cond. 
	if(RAIN<Kh){
		*INFIL=RAIN*TIME;
		return -1;
	}

	// otherwise do all this
	// Compute psi_f
	double psi_f=psi_ae*(2*b+3)/(2*b+6);
	
	// Compute charateristic time
	double CharTime=psi_f*(thi-InitWater)/Kh;

	// Compute time of ponding
	double TimePond=Kh*psi_f*(thi-InitWater)/(RAIN*(RAIN-Kh));

	// Compute Cumulative infiltration
	double CumInf=RAIN*TimePond;

	// If the time of ponding is less than the given TIME, return the 
	// amount infiltrated 
	if(TimePond>=TIME){
		*INFIL=CumInf;
		return 1;
	}

	// Otherwise, determine how much infiltrated at the given TIME
	// Compute compression time
	double TimeComp=CumInf/Kh-CharTime*log(1+CumInf/(psi_f*(thi-InitWater)));

	// compute effective time
	double effTime=TIME-TimePond+TimeComp;

	// Compute Infiltration Rate
	double ft=Kh*(0.707*sqrt((effTime+CharTime)/effTime)+0.667-0.236*sqrt(effTime/(effTime+CharTime))-0.138*(effTime/(effTime+CharTime)));

	// Compute Infiltration 
	double FT=Kh*(
		0.529*effTime+0.471*sqrt(CharTime*effTime+effTime*effTime)
		+0.138*CharTime*(log(effTime+CharTime)-log(CharTime))
		+0.471*CharTime*(log(effTime+CharTime/2+sqrt(CharTime*effTime+effTime*effTime))-log(CharTime/2))
		);
	*INFIL=FT;
	return -2;

}
/*
	function GetConnectedPoints()
	
	Description
		Finds the unoccupied connected poitns in a 1 bin radius around
		a given point. 

	INPUTS
		P			The occupancy grid containing the pool regions
		CurPoint	The point to find the connected points of
		NBINS		XYZ bins of the data
		RES			Resolution at which the data is binned
		MINS		The XYZ min values

	OUTPUTS
		cpts		A list of the connected points originating at CurPoint
					This is added to each time new points are found

	RETURNS 
		NONE
*/
template<class T>
void GetConnectedPoints(LinkedList<T>**P,T CurPoint,unsigned long int *NBINS,double *RES,double *MINS,LinkedList<T>* cpts,double **XLIMS,double **YLIMS){

	// Should be finding unoccupied connected points in a 1 bin radius
	// checks to see if it is already listed in the current list
	
	double MAXVAL=RES[2];
	T cPt(CurPoint.nXF);
	
	Node<T> *poolPtr;
	// for all neighbors in 1 bin radius
	for(int i=-1;i<=1;i++){
		for(int j=-1;j<=1;j++){
			for(int k=-1;k<=1;k++){
				cPt.x=CurPoint.x+i*RES[0];
				cPt.y=CurPoint.y+j*RES[1];
				
				// Calc bin indices
				long int xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
				long int yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);

				// if in bounds, calc z info and add to list
				if(CheckBounds(xb,yb,NBINS)&&CheckBounds(xb,yb,XLIMS,YLIMS)){
					cPt.z=CurPoint.z+k*RES[2];
					cPt.XF[0]=floor((cPt.z-MINS[2])/RES[2]+0.5);
					cPt.XF[1]=CurPoint.XF[1];

					// if it is already occupied, ignore it
					poolPtr=P[xb][yb].StPtr;
					bool occupied=false;
					while(poolPtr!=NULL){
						if(poolPtr->data.XF[0]==cPt.XF[0]){
							cPt.XF[1]=poolPtr->data.XF[1];
							if(!(cPt.XF[1]<MAXVAL)){
								occupied=true;
								//cPt.XF[1]=poolPtr->data.XF[1];
								break;
							}
						}
						poolPtr=poolPtr->next;
					}
					if(occupied==false){
						// make sure its not already listed
						poolPtr=cpts->StPtr;
						bool listed=false;
						while(poolPtr!=NULL){
							if(poolPtr->data==cPt){
								listed=true;
								break;
							}
							poolPtr=poolPtr->next;
						}
						if(listed==false){
							cPt.XF[1]=0;
							cpts->AddNode(cPt);
						}
					}else{
						// see if it is full
					//	if(cPt.XF[1]<MAXVAL){
					//		cpts->AddNode(cPt);
					//	}
					}
				}
			}
		}
	}
}

/*
	function CheckConnectedPoints()
	
	Description
		Checks a connected points list to determine if any of the points
		have become occupied. If a point is occupied, it is removed from
		the list and its connected points are added.

	INPUTS
		P			The occupancy grid containing the pool regions
		cpts		A list of the connected points
		NBINS		XYZ bins of the data
		RES			Resolution at which the data is binned
		MINS		The XYZ min values

	OUTPUTS
		cpts		The connected points list with occupied points
					removed

	RETURNS 
		NONE
*/
template <class T>
void CheckConnectedPoints(LinkedList<T>**P,LinkedList<T> *cpts,unsigned long int *NBINS, double *RES,double *MINS,double **XLIMS,double **YLIMS){

	double MAXVAL=RES[2];
	Node<VOXEL_N> *cptr;
	Node<VOXEL_N> *poolPtr;

	// check connected points for occupancy
	cptr=cpts->StPtr;
	while(cptr!=NULL){
		bool remstart=false;
		long int xb=(long int)floor((cptr->data.x-MINS[0])/RES[0]+0.5);
		long int yb=(long int)floor((cptr->data.y-MINS[1])/RES[1]+0.5);
		if(CheckBounds(xb,yb,NBINS)&&CheckBounds(xb,yb,XLIMS,YLIMS)){
			poolPtr=P[xb][yb].StPtr;
			while(poolPtr!=NULL){
				if(poolPtr->data.XF[0]==cptr->data.XF[0]){
					cptr->data.XF[1]=poolPtr->data.XF[1];
					// it is occupied at this point
					// see if it is full
					if(!(cptr->data.XF[1]<MAXVAL)){
						// need to find this points unoccupied connect points
						GetConnectedPoints(P,cptr->data,NBINS,RES,MINS,cpts,XLIMS,YLIMS);
					
						// delete this point
						if(cptr==cpts->StPtr){
							remstart=true;
						}
						cptr=cpts->DelNode(cptr);
					}

				}
				poolPtr=poolPtr->next;
			}
		}
		if(remstart==false){
			cptr=cptr->next;
		}
	}
}
/*
	function RemLowConnectedPoints()
	
	Description
		Removes connected points that are lower than the given surface

	INPUTS
		P			The occupancy grid containing the pool regions
		cpts		A list of the connected points
		NBINS		XYZ bins of the data
		RES			Resolution at which the data is binned
		MINS		The XYZ min values

	OUTPUTS
		cpts		The connected points list with occupied points
					removed

	RETURNS 
		NONE
*/
template<class T>
void RemLowConnectedPoints(LinkedList<T> **B,LinkedList<T> *cpts,unsigned long int *NBINS, double *RES,double *MINS,double **XLIMS,double **YLIMS){
	
	Node<T> *cptr2;
	cptr2=cpts->StPtr;
	while(cptr2!=NULL){
		bool remstart=false;
		long int xb=(long int)floor((cptr2->data.x-MINS[0])/RES[0]+0.5);
		long int yb=(long int)floor((cptr2->data.y-MINS[1])/RES[1]+0.5);
		if(CheckBounds(xb,yb,NBINS)&&CheckBounds(xb,yb,XLIMS,YLIMS)){
			if(B[xb][yb].StPtr!=NULL){
				if(cptr2->data.z<=B[xb][yb].StPtr->data.z){
					if(cptr2==cpts->StPtr){
						remstart=true;
					}
					cptr2=cpts->DelNode(cptr2);
				}
			}
		}
		if(remstart==false){
			cptr2=cptr2->next;
		}
	}
}
/*
	function WaterFlow()
	
	Description
		The workhorse of the hydrology aspect of this code. It determines 
		where the water will flow and keeps track of where it pools within
		the gridded data. 

	INPUTS
		B			The grid contianing point data of which to model the water
		NBINS		XYZ bins of the data
		RES			Resolution at which the data is binned
		MINS		The XYZ min values

	OUTPUTS
		FILE		A file contianing the pooled data
					TODO: make this a return of the function
		B.XF[1]		The second extra field of the input grid is a map
					which represents the water flow paths 

	RETURNS 
		NONE
*/
template<class T>
void WaterFlow(LinkedList<T>**B,LinkedList<T>**POOL,unsigned long int *NBINS,double *RES,double *MINS, int SM[313][313],double *SMSTEP, double RainFallRate, double TIME){

	Node<VOXEL_N> *tmpPtr;
	VOXEL_N poolPt;
	
	double pts[3][3];
	bool *visited=new bool[NBINS[0]*NBINS[1]];

	LinkedList<VOXEL_N> **CPTSPOOL=AllocateGrid<VOXEL_N>(NBINS[0],NBINS[1]);;	
	VOXEL_N cPt(2);

	double **xlims=new double*[2];
	for(int i=0;i<2;i++){
		xlims[i]=new double[NBINS[0]];
		memset(xlims[i],0,sizeof(double)*NBINS[0]);
	}
	double **ylims=new double*[2];
	for(int i=0;i<2;i++){
		ylims[i]=new double[NBINS[1]];
		memset(ylims[i],0,sizeof(double)*NBINS[1]);
	}
	for(int i=0;i<NBINS[0];i++){
		xlims[0][i]=1e23;
		xlims[1][i]=-1e23;
	}
	for(int i=0;i<NBINS[1];i++){
		ylims[0][i]=1e23;
		ylims[1][i]=-1e23;
	}

	GetDataBounds(B,NBINS,xlims,ylims);

	// Get latitude of box (use the min value)
	// The data in the northing direction is wrong. Using the 
	// lower left bounding box from the XML description file for now
	// Eventually, it might be good to convert that to UTM, assuming it
	// is the point for the xy min, and add each voxels distance to it
	// to get the 'exact' value. Considering this is used for calculating
	// day length though, probably don't even need to worry over the small
	// areas considered here. 

	// (lat,Average Daily Temp,Day Number)
	double PET=CalcPotentialEvap(38.4687500000276,24,180);
	PET/=(1000.0*24.0);//convert to m/hr

	double MaxWaterVolume=RES[0]*RES[1]*RES[2];//RES[0]*RES[1]*RES[2];	// maximum amount of water able to be stored
//	double RainFallRate=0.20; // meters per Sq_meter
//	double TIME=3;
	double CurAW=0;
	double BulkDen[19];
	BulkDen[0]=1.25/(1000*1e-6);
	BulkDen[1]=1.4/(1000*1e-6);
	BulkDen[2]=1.33/(1000*1e-6);
	BulkDen[3]=1.25/(1000*1e-6);
	BulkDen[4]=1.45/(1000*1e-6);
	BulkDen[5]=1.5/(1000*1e-6);
	BulkDen[6]=1.45/(1000*1e-6);
	BulkDen[7]=1.4/(1000*1e-6);
	BulkDen[8]=1.35/(1000*1e-6);
	BulkDen[9]=1.27/(1000*1e-6);
	BulkDen[10]=1.27/(1000*1e-6);
	BulkDen[11]=1.35/(1000*1e-6);
	BulkDen[12]=1.35/(1000*1e-6);
	BulkDen[13]=1.3/(1000*1e-6);
	BulkDen[14]=1.3/(1000*1e-6);
	BulkDen[15]=1.52/(1000*1e-6);
	BulkDen[16]=1.52/(1000*1e-6);
	BulkDen[17]=1.2/(1000*1e-6);
	BulkDen[18]=1.2/(1000*1e-6);

	// CPTSPOOL
	//		currently 1 extra field (2nd to be added soon)
	//			1st		Z index of point
	//			2nd		Water Content
	//

	
	double TotalWater=0;
	double AddedGroundWater=0;
	double EvapLoss=0;
	double OffMapLoss=0;
	
	printf("\n                  ");    
	long int cnt=0;
	int count=0;	
	double REM2=0;
		
	// The main loop 'flows' the water
	for(unsigned long int x=0;x<NBINS[0];x++){
		for(unsigned long int y=0;y<NBINS[1];y++){
			bool DoPooling=false;

			// displays a counter indicating progress
			printf("\b\b\b\b\b\b\b%0.5f",(double)cnt/(NBINS[1]*NBINS[0]));
		
			// The 'bare earth' function put the BE estimate in the first node of each column
			// so only need to use the start pointer
			tmpPtr=B[x][y].StPtr;
		
			// Keep track of how much water is added
			if(tmpPtr!=NULL){
				//CurAW=RainFallRate*TIME;
				//TotalWater+=(RainFallRate*TIME);
				CurAW=RainFallRate*TIME*(RES[0]*RES[1]);
				TotalWater+=(RainFallRate*TIME*(RES[0]*RES[1]));
				//
				//	account for evaporation loss
				//
				double EVAP=PET*TIME*(RES[0]*RES[1]);
				if((CurAW-EVAP)>0){
					CurAW-=EVAP;
					EvapLoss+=EVAP;
				}else{
					// out of water, exit loop
					EvapLoss+=CurAW;
					CurAW=0;
					break;
				}
			}
			
			int nx=x;
			int ny=y;

			memset(visited,0,sizeof(bool)*NBINS[0]*NBINS[1]);
						
			// Follows a flow path to its 'end', then indicates if pooling needs to happen
			while(tmpPtr!=NULL){
				if(visited[NBINS[0]*nx+ny]==false){
					visited[NBINS[0]*nx+ny]=true;
				}else{
					// this means we circled back to the current path, so get out
					DoPooling=true;
					break;
				}

				count++;	// Used to calculate the total rainfall over the area

				// Increase water counter 
				tmpPtr->data.XF[1]+=1;
				
				//
				//	account for water into ground
				//
				int sxInd=(tmpPtr->data.x-MINS[0])/SMSTEP[0];
				int syInd=(tmpPtr->data.y-MINS[1])/SMSTEP[1];
				double INFIL=0;
				double Vs=(RES[0]*RES[1]*RES[2])*(BulkDen[SM[sxInd][syInd]]/2650);
				//double tmpThi=0;
				//double ttVal;
				//GetSoilParameters(SM[sxInd][syInd],&tmpThi,&ttVal,&ttVal,&ttVal);
				//double Vs=(RES[0]*RES[1]*RES[2])*(1-tmpThi);
				double Vw=tmpPtr->data.XF[2];
				double THETA=Vw/Vs;
				int PN=GreenAmpt(RainFallRate*100/TIME,TIME,THETA,SM[sxInd][syInd],&INFIL);
				INFIL/=100;
				INFIL*=(RES[0]*RES[1]);
				if((CurAW-INFIL)>0){
					CurAW-=INFIL;
					tmpPtr->data.XF[2]+=INFIL;
					AddedGroundWater+=INFIL;
				}else{
					// ran out of water, exit loop
					tmpPtr->data.XF[2]+=CurAW;
					AddedGroundWater+=CurAW;
					CurAW=0;
					break;
				}
		
				// 
				//	The remaining water is flowed following surface gradients
				//
				memset(pts,0,sizeof(double)*9);
				
				// finds the neighborhood points, if the is no neighbor
				// make that point equal to the center point
				for(int r=-1;r<=1;r++){
					for(int c=-1;c<=1;c++){
						if(CheckBounds(nx+c,ny+r,NBINS)&&CheckBounds(nx+c,ny+r,xlims,ylims)){
							if(B[nx+c][ny+r].StPtr!=NULL){
								pts[r+1][c+1]=B[nx+c][ny+r].StPtr->data.z;
							}else{
								pts[r+1][c+1]=B[nx][ny].StPtr->data.z;
							}
						}else{
							pts[r+1][c+1]=B[nx][ny].StPtr->data.z;
						}
					}
				}
			
				// check for local minimum
				double minval=1e10;
				for(int r=0;r<=2;r++){
					for(int c=0;c<=2;c++){
						if(pts[r][c]<minval){
							minval=pts[r][c];
						}
					}
				}
				
				// Local Minimum
				if(minval==pts[1][1]){
					// begin pooling
					DoPooling=true;
					break;
				}
				
				// Calculates the gradients
				double delta[4];
				memset(delta,0,sizeof(double)*4);
				delta[0]=(pts[1][2]-pts[1][0])/2;
				delta[1]=(pts[2][1]-pts[0][1])/2;
				delta[2]=(pts[2][2]-pts[0][0])/sqrt(8.0);
				delta[3]=(pts[2][0]-pts[0][2])/sqrt(8.0);

				// find the maximum gradient
				double maxval=-1e10;
				int maxind=-1;
				for(int i=0;i<4;i++){
					if(abs((double)delta[i])>(double)maxval){
						maxval=abs(delta[i]);
						maxind=i;
					}
				}
				
				// determine next point
				if((maxind>-1)){
					switch(maxind){
						case 0:
							nx+=(int)(-1*delta[maxind]/abs((double)delta[maxind]));
							break;
						case 1:
							ny+=(int)(-1*delta[maxind]/abs((double)delta[maxind]));
							break;
						case 2:
							nx+=(int)(-1*delta[maxind]/abs((double)delta[maxind]));
							ny+=(int)(-1*delta[maxind]/abs((double)delta[maxind]));
							break;
						case 3:
							nx+=(int)(-1*delta[maxind]/abs((double)delta[maxind]));
							ny+=(int)(1*delta[maxind]/abs((double)delta[maxind]));
							break;
					}

					if(CheckBounds(nx,ny,NBINS)&&CheckBounds(nx,ny,xlims,ylims)){
						tmpPtr=B[nx][ny].StPtr;
					}else{
						// off the map, get out
						OffMapLoss+=CurAW;
						CurAW=0;
						break;
					}
				}else{
					// never seems to get here, so can remove this if sometime
					break;
				}
			}
			
			// Pooling happens here if indicated
			if(DoPooling==true){
				// begin pooling
				// set current pooling point
				poolPt.x=nx*RES[0]+MINS[0];
				poolPt.y=ny*RES[1]+MINS[1];
				poolPt.z=tmpPtr->data.z+RES[2];
				poolPt.nXF=2;
				poolPt.AllocateXFmem(2);
				poolPt.XF[0]=floor((poolPt.z-MINS[2])/RES[2]+0.5);
				poolPt.XF[1]=0;
				
				GetConnectedPoints(POOL,poolPt,NBINS,RES,MINS,&CPTSPOOL[nx][ny],xlims,ylims);
			
				// eliminate points lower than surface 
				RemLowConnectedPoints(B,&CPTSPOOL[nx][ny],NBINS,RES,MINS,xlims,ylims);
				
				// select the lowest remaining connected point
				if(CPTSPOOL[nx][ny].NumNodes>0){
					SortZColumn(&CPTSPOOL[nx][ny],1);
					cPt=CPTSPOOL[nx][ny].StPtr->data;
					double REM3=0;
					if((cPt.XF[1]+CurAW)<=MaxWaterVolume){
						// Select the lowest and add it
						cPt=CPTSPOOL[nx][ny].StPtr->data;
						cPt.XF[1]+=CurAW;
						long int xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
						long int yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);
						POOL[xb][yb].AddNode(cPt);
					}else{
						// How many voxels of water to add?
						int nvox=(int)floor(CurAW/(MaxWaterVolume));

						for(int i=0;i<nvox;i++){
							// Sort the connected poitns by z value
							SortZColumn(&CPTSPOOL[nx][ny],1);
							
							// select the lowest point
							cPt=CPTSPOOL[nx][ny].StPtr->data;
							if(cPt.XF[1]<=MaxWaterVolume){
								REM3+=cPt.XF[1];
							}
							cPt.XF[1]=MaxWaterVolume;
							
							// add it to the pool
							long int xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
							long int yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);
							POOL[xb][yb].AddNode(cPt);

							GetConnectedPoints(POOL,cPt,NBINS,RES,MINS,&CPTSPOOL[nx][ny],xlims,ylims);
							RemLowConnectedPoints(B,&CPTSPOOL[nx][ny],NBINS,RES,MINS,xlims,ylims);
						}
						// if there is a remainder left, add it in
						// need to add the initial amount + the remainder
						double REM=(CurAW-nvox*MaxWaterVolume)+REM3;
						if(REM>0){
							SortZColumn(&CPTSPOOL[nx][ny],1);
							// select the lowest point
							cPt=CPTSPOOL[nx][ny].StPtr->data;
							if((REM+cPt.XF[1])<=MaxWaterVolume){
								cPt.XF[1]+=REM;
								long int xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
								long int yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);
								POOL[xb][yb].AddNode(cPt);

								GetConnectedPoints(POOL,cPt,NBINS,RES,MINS,&CPTSPOOL[nx][ny],xlims,ylims);
								RemLowConnectedPoints(B,&CPTSPOOL[nx][ny],NBINS,RES,MINS,xlims,ylims);
							}else{
								cPt.XF[1]=MaxWaterVolume;
								long int xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
								long int yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);
								POOL[xb][yb].AddNode(cPt);

								GetConnectedPoints(POOL,cPt,NBINS,RES,MINS,&CPTSPOOL[nx][ny],xlims,ylims);
								RemLowConnectedPoints(B,&CPTSPOOL[nx][ny],NBINS,RES,MINS,xlims,ylims);
								SortZColumn(&CPTSPOOL[nx][ny],1);
								cPt=CPTSPOOL[nx][ny].StPtr->data;
								cPt.XF[1]+=REM;
								xb=(long int)floor((cPt.x-MINS[0])/RES[0]+0.5);
								yb=(long int)floor((cPt.y-MINS[1])/RES[1]+0.5);
								POOL[xb][yb].AddNode(cPt);
							}
						}
					}
				}
			}
			cnt++;
		}
	}
	
	DeAllocateGrid(CPTSPOOL,NBINS[0]);
	delete [] visited;

	for(int i=0;i<2;i++){
		delete [] xlims[i];
	}
	delete [] xlims;
	for(int i=0;i<2;i++){
		delete [] ylims[i];
	}
	delete [] ylims;

	double PCU=0;
	for(int x=0;x<NBINS[0];x++){
		for(int y=0;y<NBINS[1];y++){
			tmpPtr=POOL[x][y].StPtr;
			while(tmpPtr!=NULL){
				PCU+=tmpPtr->data.XF[1];
				tmpPtr->data.z+=RES[2];
				tmpPtr=tmpPtr->next;
			}
		}
	}

	for(int x=0;x<NBINS[0];x++){
		for(int y=0;y<NBINS[1];y++){
			tmpPtr=B[x][y].StPtr;
			while(tmpPtr!=NULL){
				int sxInd=(tmpPtr->data.x-MINS[0])/SMSTEP[0];
				int syInd=(tmpPtr->data.y-MINS[1])/SMSTEP[1];
				double tmpThi=0;
				double ttVal;
				//GetSoilParameters(SM[sxInd][syInd],&tmpThi,&ttVal,&ttVal,&ttVal);
				double Vs=(RES[0]*RES[1]*RES[2])*(BulkDen[SM[sxInd][syInd]]/2650);
				//double Vs=RES[0]*RES[1]*RES[2]*(1-tmpThi);
				tmpPtr->data.XF[2]=tmpPtr->data.XF[2]/Vs;//THETA/BulkDen[SM[sxInd][syInd]];
				tmpPtr=tmpPtr->next;
			}
		}
	}

	cout.setf(ios::fixed,ios::floatfield);
	cout<<"\n";
	cout<<"Total RainFall     (m3)      :\t"<<TotalWater<<"\n";
	cout<<"Rain fall rate     (m/hr)   :\t"<<RainFallRate<<"\n";
	cout<<"Rain Time          (hr)     :\t"<<TIME<<"\n";
	cout<<"Potential Evap.    (m/hr)   :\t"<<PET<<"\n";
	cout<<"Water Evaporated   (m3)      :\t"<<EvapLoss<<"\n";
	cout<<"Ground Water Added (m3)      :\t"<<AddedGroundWater<<"\n";
	cout<<"Water Off Map      (m3)      :\t"<<OffMapLoss<<"\n";
	cout<<"Pooled Water       (m3)      :\t"<<PCU<<"\n";
	cout<<"TotalWater (check) (m3)      :\t"<<PCU+OffMapLoss+AddedGroundWater+EvapLoss<<"\n";
	cout<<"Expected-modeled check\n";
	cout<<TotalWater-(PCU+OffMapLoss+AddedGroundWater+EvapLoss)<<"\n";
}
#endif