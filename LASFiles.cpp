/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/
#include <fstream>
#include "LASFiles.h"

using namespace std;

LAS_HEADER::LAS_HEADER(){
	sprintf(file_sig,"    ");
	source_id=0;
	global_enc=0;
	guid1=0;
	guid2=0;
	guid3=0;
	ver_maj=0;
	ver_min=0;
	for(int i=0;i<32;i++){	sprintf(sys_ident," ");
	sprintf(generator," ");}
	create_doy=0;
	create_year=0;
	head_size=0;
	point_offset=0;
	num_var=0;
	point_format=0;
	point_len=0;
	num_points=0;
	for(int i=0;i<5;i++){	num_ret[i]=0;}
	x_scale=0;
	y_scale=0;
	z_scale=0;
	x_off=0;
	y_off=0;
	z_off=0;
	x_max=0;
	x_min=0;
	y_max=0;
	y_min=0;
	z_max=0;
	z_min=0;
}
void LASFile::ReadLASHeader(){

	ifstream IN(FileName,ios::binary);
	
	IN.read((char*)&Header.file_sig,sizeof(char)*4);
	IN.read((char*)&Header.source_id,sizeof(unsigned short int));
	IN.read((char*)&Header.global_enc,sizeof(unsigned short int));
	IN.read((char*)&Header.guid1,sizeof(unsigned long int));
	IN.read((char*)&Header.guid2,sizeof(unsigned short int));
	IN.read((char*)&Header.guid3,sizeof(unsigned short int));
	IN.read((char*)&Header.guid4,sizeof(unsigned char)*8);
	IN.read((char*)&Header.ver_maj,sizeof(unsigned char));
	IN.read((char*)&Header.ver_min,sizeof(unsigned char));
	IN.read((char*)&Header.sys_ident,sizeof(char)*32);
	IN.read((char*)&Header.generator,sizeof(char)*32);
	IN.read((char*)&Header.create_doy,sizeof(unsigned short int));
	IN.read((char*)&Header.create_year,sizeof(unsigned short int));
	IN.read((char*)&Header.head_size,sizeof(unsigned short int));
	IN.read((char*)&Header.point_offset,sizeof(unsigned long int));
	IN.read((char*)&Header.num_var,sizeof(unsigned long int));
	IN.read((char*)&Header.point_format,sizeof(unsigned char));
	IN.read((char*)&Header.point_len,sizeof(unsigned short int));
	IN.read((char*)&Header.num_points,sizeof(unsigned long int));
	IN.read((char*)&Header.num_ret,sizeof(unsigned long int)*5);
	IN.read((char*)&Header.x_scale,sizeof(double));
	IN.read((char*)&Header.y_scale,sizeof(double));
	IN.read((char*)&Header.z_scale,sizeof(double));
	IN.read((char*)&Header.x_off,sizeof(double));
	IN.read((char*)&Header.y_off,sizeof(double));
	IN.read((char*)&Header.z_off,sizeof(double));
	IN.read((char*)&Header.x_max,sizeof(double));
	IN.read((char*)&Header.x_min,sizeof(double));
	IN.read((char*)&Header.y_max,sizeof(double));
	IN.read((char*)&Header.y_min,sizeof(double));
	IN.read((char*)&Header.z_max,sizeof(double));
	IN.read((char*)&Header.z_min,sizeof(double));
	
	IN.close();
}

void LASFile::DisplayHeader(){
	printf("file_sig\t%s\n",Header.file_sig);
	printf("source_id\t%d\n",Header.source_id);
	printf("global_enc\t%d\n",Header.global_enc);
	printf("guid1\t%d\n",Header.guid1); 
	printf("guid2\t%d\n",Header.guid2); 
	printf("guid3\t%d\n",Header.guid3); 
	printf("guid4\t%s\n",Header.guid4); 
	printf("ver_maj\t%u\n",Header.ver_maj);
	printf("ver_min\t%u\n",Header.ver_min);
	printf("sys_ident\t%s\n",Header.sys_ident);
	printf("generator\t%s\n",Header.generator);
	printf("create_doy\t%d\n",Header.create_doy);
	printf("create_year\t%d\n",Header.create_year);
	printf("head_size\t%d\n",Header.head_size); 
	printf("point_offset\t%d\n",Header.point_offset);
	printf("num_var\t%d\n",Header.num_var); 
	printf("point_format\t%u\n",Header.point_format);
	printf("point_len\t%d\n",Header.point_len); 
	printf("num_points\t%d\n",Header.num_points);
	for(int i=0;i<5;i++){
		printf("num_ret\t%d\n",Header.num_ret[i]); 
	}
	printf("x_scale\t%e\n",Header.x_scale); 
	printf("y_scale\t%e\n",Header.y_scale); 
	printf("z_scale\t%e\n",Header.z_scale); 
	printf("x_off\t%f\n",Header.x_off); 
	printf("y_off\t%f\n",Header.y_off); 
	printf("z_off\t%f\n",Header.z_off); 
	printf("max_x\t%f\n",Header.x_max); 
	printf("min_x\t%f\n",Header.x_min); 
	printf("max_y\t%f\n",Header.y_max); 
	printf("min_y\t%f\n",Header.y_min); 
	printf("max_z\t%f\n",Header.z_max); 
	printf("min_z\t%f\n",Header.z_min); 
}
void LASFile::ReadLASData(){

	ifstream IN(FileName,ios::binary);
	
	ReadLASHeader();

	data=new double*[4];
	for(int i=0;i<4;i++){
		data[i]=new double[Header.num_points];
	}
	IN.seekg(Header.point_offset);
	//IN.seekg(Header.head_size+2);
	long int tmp;
	unsigned short int tmp2;
	
	for(unsigned long int n=0;n<Header.num_points;n++){
		IN.read((char*)&tmp,sizeof(long int));
		data[0][n]=((double)tmp)*Header.x_scale+Header.x_off;
		IN.read((char*)&tmp,sizeof(long int));
		data[1][n]=((double)tmp)*Header.y_scale+Header.y_off;
		IN.read((char*)&tmp,sizeof(long int));
		data[2][n]=((double)tmp)*Header.z_scale+Header.z_off;
		IN.read((char*)&tmp2,sizeof(unsigned short int));
		data[3][n]=((float)tmp2);
		IN.seekg(Header.point_len-14,ios::cur);
	}
}

void LASFile::WriteLASData(char * APPEND){
	strcat(FileName,APPEND);
	
	ofstream OUT(FileName,ios::binary);
	Header.point_offset=Header.head_size+1;
	Header.num_var=0;
	OUT.write((char*)&Header.file_sig,sizeof(char)*4);
	OUT.write((char*)&Header.source_id,sizeof(unsigned short int));
	OUT.write((char*)&Header.global_enc,sizeof(unsigned short int));
	OUT.write((char*)&Header.guid1,sizeof(unsigned long int));
	OUT.write((char*)&Header.guid2,sizeof(unsigned short int));
	OUT.write((char*)&Header.guid3,sizeof(unsigned short int));
	OUT.write((char*)&Header.guid4,sizeof(unsigned char)*8);
	OUT.write((char*)&Header.ver_maj,sizeof(unsigned char));
	OUT.write((char*)&Header.ver_min,sizeof(unsigned char));
	OUT.write((char*)&Header.sys_ident,sizeof(char)*32);
	OUT.write((char*)&Header.generator,sizeof(char)*32);
	OUT.write((char*)&Header.create_doy,sizeof(unsigned short int));
	OUT.write((char*)&Header.create_year,sizeof(unsigned short int));
	OUT.write((char*)&Header.head_size,sizeof(unsigned short int));
	OUT.write((char*)&Header.point_offset,sizeof(unsigned long int));
	OUT.write((char*)&Header.num_var,sizeof(unsigned long int));
	OUT.write((char*)&Header.point_format,sizeof(unsigned char));
	OUT.write((char*)&Header.point_len,sizeof(unsigned short int));
	OUT.write((char*)&Header.num_points,sizeof(unsigned long int));
	OUT.write((char*)&Header.num_ret,sizeof(unsigned long int)*5);
	OUT.write((char*)&Header.x_scale,sizeof(double));
	OUT.write((char*)&Header.y_scale,sizeof(double));
	OUT.write((char*)&Header.z_scale,sizeof(double));
	OUT.write((char*)&Header.x_off,sizeof(double));
	OUT.write((char*)&Header.y_off,sizeof(double));
	OUT.write((char*)&Header.z_off,sizeof(double));
	OUT.write((char*)&Header.x_max,sizeof(double));
	OUT.write((char*)&Header.x_min,sizeof(double));
	OUT.write((char*)&Header.y_max,sizeof(double));
	OUT.write((char*)&Header.y_min,sizeof(double));
	OUT.write((char*)&Header.z_max,sizeof(double));
	OUT.write((char*)&Header.z_min,sizeof(double));
	
	OUT.seekp(Header.point_offset,ios::beg);
	
	long int tmp;
	unsigned short int tmp2;
	unsigned char tmp3=0;
	for(unsigned long int n=0;n<Header.num_points;n++){
		long int h=OUT.tellp();
		tmp=(long int)((data[0][n]-Header.x_off)/Header.x_scale);
		OUT.write((char*)&tmp,sizeof(long int));
				
		tmp=(long int)((data[1][n]-Header.y_off)/Header.y_scale);
		OUT.write((char*)&tmp,sizeof(long int));
		
		tmp=(long int)((data[2][n]-Header.z_off)/Header.z_scale);
		OUT.write((char*)&tmp,sizeof(long int));
		
		tmp2=(unsigned short int)(data[3][n]);
		OUT.write((char*)&tmp2,sizeof(unsigned short int));
		
		for(int i=0;i<Header.point_len-14;i++){
			OUT.write((char*)&tmp3,sizeof(unsigned char));
		}
	}
	
	OUT.close();
}