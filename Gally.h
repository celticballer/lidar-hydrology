/*

	Copyright (C) 2012 Andrew J Gallagher (andrew.j.gallagher@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to 
deal in the Software without restriction, including without limitation the 
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
THE SOFTWARE.

*/
/*************************************************************************

	A collection of random routines used sporadically throughout the code. 

*************************************************************************/

#ifndef GALLY_H
#define GALLY_H
/*
	function BubbleSort()

	Description
		A bubble sort algorithm

	INPUTS
		data	array containing the data to be sorted
		sz		the size of the array

	OUTPUTS
		data	the sorted result

	RETURNS
		NONE
*/
void BubbleSort(double *data,int sz){
	double tmp=0;
	bool swap=true;
	int cnt=0;
	while(1){
		swap=false;
		for(int i=0;i<sz-1;i++){
			if(data[i]>data[i+1]){
				tmp=data[i+1];
				data[i+1]=data[i];
				data[i]=tmp;
				swap=true;
			}
		}
		if(swap==false){
			break;
		}
		cnt++;
	}
}
/*
	function Median()

	Description
		Determines the median value of an array

	INPUTS
		V		data to find the mean of
		sz		size of the data

	OUTPUTS
		NONE

	RETURNS
		the median value of the data

*/
template<class T>
T Median(T *V,int sz){
	double Tm=0;
	switch(sz){
		case 1:
			Tm=V[0];
			break;
		case 2:
			Tm=(V[0]+V[1])/2;
			break;
		default:
			BubbleSort(V,sz);
			if(sz%2==1){
				Tm=V[(sz+1)/2];
			}else{
				Tm=(V[sz/2-1]+V[sz/2])/2;
			}
			break;
	}
	return Tm;
}
#endif